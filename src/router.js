import { createWebHistory, createRouter} from "vue-router";

import CreateEvent from "./pages/event/CreateEvent";
import EventPage from "./pages/event/EventPage";
import UserDashboard from "./pages/user/UserDashboard";
import UserProfile from "./pages/user/UserProfile";
import Home from "./pages/Home";
import EventList from "./pages/event/EventList";
import UserRegistration from "./pages/user/UserRegistration";
import NotFound from "./pages/NotFound";
import UserLogin from "./pages/user/UserLogin";
import SearchBar from "./components/SearchBar";
import TheMap from "./components/TheMap";
import SearchAddress from "./components/SearchAddress";
import TheFooter from "./components/TheFooter";
import HereMap from "./components/HereMap";

const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/createevent',
        component: CreateEvent
    },
    {
        path: '/event', component: EventPage
    },
    {
        path: '/register', component: UserRegistration
    },
    {
        path: '/userdashboard', component: UserDashboard
    },
    {
        path: '/userprofile', component: UserProfile
    },
    {
        path: '/userlogin', component: UserLogin
    },
    {
        path: '/eventlist', component: EventList
    },
    {
        path: '/searchbar', component: SearchBar
    },
    {
        path: '/map', component: TheMap
    },
    {
        path: '/heremap',
        component: HereMap
    },
    {
        path: '/search', component: SearchAddress
    },
    {
        path: '/footer', component: TheFooter
    },
    {
        path: '/:notFound(.*)', component: NotFound
    }
    // {
    //     path: '/coaches/:id', component: null, children: [
    //         {path: 'contect', component: null},
    //     ]
    // },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;