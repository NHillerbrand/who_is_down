import { createStore } from "vuex";

import eventsModule from "./modules/events";
import userModule from "./modules/users";


const store = createStore({
    users: userModule,
    events: eventsModule
});

export default store;