export default {
    namespaced: true,
    state() {
        return {
            users: [
                {
                    "userId": 1000000001,
                    "userName": "TestName1",
                    "mail": "test@gmail.com"
                },
                {
                    "userId": 1000000002,
                    "userName": "TestName2",
                    "mail": "test@gmail.com"
                },
                {
                    "userId": 1000000003,
                    "userName": "TestName2",
                    "mail": "test@gmail.com"
                }
            ]
        }
    },
    getters: {
        users(state) {
            return state.users;
        }
    }
}