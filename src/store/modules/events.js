export default {
    namespaced: true,
    state() {
        return {
            events: [
                {
                    "eventId": 10000003,
                    "title": "Yoga",
                    "description": "Mega Yoga Beschreibung",
                    "location": "Mauerpark",
                    "coordinates": [13.45970, 52.51092],
                    "public": true,
                    "hostId": 1000001,
                    "date": "2012-03-19T07:22Z",
                    "maxGuests": 15,
                    "link": "https://png.pngitem.com/pimgs/s/207-2073499_translate-platform-from-english-to-spanish-work-in.png"
                },
                {
                    "eventId": 10000004,
                    "title": "Yoga",
                    "description": "Mega Yoga Beschreibung",
                    "location": "Mauerpark",
                    "coordinates": [13.466759986509386,52.519235796332666],
                    "public": true,
                    "hostId": 1000001,
                    "date": "2012-03-19T07:22Z",
                    "maxGuests": 15,
                    "link": "https://png.pngitem.com/pimgs/s/207-2073499_translate-platform-from-english-to-spanish-work-in.png"
                },
                {
                    "eventId": 10000006,
                    "title": "Brauhaus Neulich",
                    "description": "Mega Yoga Beschreibung",
                    "location": "Mauerpark",
                    "coordinates": [ 13.41994259816499, 52.47844221456504],
                    "public": true,
                    "hostId": 1000001,
                    "date": "2012-03-19T07:22Z",
                    "maxGuests": 15,
                    "link": "https://png.pngitem.com/pimgs/s/207-2073499_translate-platform-from-english-to-spanish-work-in.png"
                }
            ]
        }
    },
    getters: {
        events(state) {
            return state.events;
        }
    }
}
